import random


def get_all(action, limit=50, **kwargs):
    """ Generator - TODO: ... """
    obtain = True
    offset = 0
    while obtain:
        items = action(limit=limit, offset=offset, **kwargs)
        for i in items["items"]:
            yield i
        if (offset + len(items["items"])) == items["total"]:
            obtain = False
        else:
            offset += limit

def create_chunks(list_data, chunk_size):
    """ """
    while True:
        chunk = []
        try:
            for i in range(chunk_size):
                val = next(list_data)
                chunk.append(val)
        except StopIteration:
            if chunk:
                yield chunk
            break
        yield chunk

def get_items(item_getter, data_num:int, shuffled=False, repetition=False, cyclic=False, limit:int=None):
    """ Allow to get items in multiple output order
        Generator

        Arguments:
            item_getter - function that has 1 parameter (index) and will return element at passed index
            data_num    - number of processed elements
            shuffled    - if True tracks will be returned in random order
            repetition  - repetition of tracks allowed
            cyclic      - all values needs to be returned before they will start repeating themselves
            limit       - set limit of data to get, if None, data will be returned forever (with repetitions)
    """
    if data_num <= 0:
        return
        # raise Exception("Playlist is empty.")
    if (not repetition) and limit and (limit > data_num):
        raise ValueError("Passed parameters repetition and limit combination is forbidden.")

    free = []
    while True:
        # shuffled and limit with passed values
        if not (limit is None):
            if limit <= 0:
                return
            elif limit > 0:
                limit -= 1

        if repetition and (not cyclic) and shuffled:
            # values can repeat themselves
            idx = random.randint(0, data_num - 1)

        else:
            # values can repeat themselves but not inside same group
            # values can not repeat themselves
            try:
                idx = free[0]
            except IndexError:
                free = list(range(data_num))
                if shuffled:
                    random.shuffle(free)
                idx = free[0]

            del free[0]

        yield item_getter(idx)
