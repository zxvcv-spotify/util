Changelog
=========

0.0.1 (2021-12-12)
------------------
- Move _util functions from spotify.core:
  get_all(), create_chunks(), get_items()

0.0.0 (2021-12-12)
------------------
- Initial commit.
